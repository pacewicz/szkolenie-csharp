﻿using System;
using System.Threading.Tasks;

namespace DependencyInjectionIntro
{
	public interface IMessageSendingService
	{
		void Send(Message m);
	}

	public class ConsoleMessageSendingService : IMessageSendingService
	{
		private readonly Random _rnd = new Random();

		public async void Send(Message m)
		{
			await Task.Delay(3*_rnd.Next(1000, 5000));
			Console.WriteLine(GetHashCode()+"> "+m.Text);
		}
	}

	public class StarConsoleMessageSendingService : IMessageSendingService
	{
		private readonly Random _rnd = new Random();

		public async void Send(Message m)
		{
			await Task.Delay(3 * _rnd.Next(1000, 5000));
			Console.WriteLine(GetHashCode() + "* " + m.Text);
		}
	}
}