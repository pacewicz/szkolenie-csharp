﻿using System.Collections.Generic;
using System.IO;

namespace DependencyInjectionIntro
{
	public interface IMessagePersistenceService
	{
		void SaveAll(IEnumerable<Message> m);
	}

	public class FileMessagePersistenceService : IMessagePersistenceService
	{
		public void SaveAll(IEnumerable<Message> messages)
		{
			const string filePath = "messages.json";
			if(File.Exists(filePath))
			{
				File.Delete(filePath);
			}
			var file = File.CreateText(filePath);
		}
	}
}