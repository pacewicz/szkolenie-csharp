﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;

namespace DependencyInjectionIntro
{
	class Program
	{
		private static IUnityContainer _container;

		static void Bootstrap()
		{
			_container = new UnityContainer();
			_container.RegisterType<IMessagePersistenceService, FileMessagePersistenceService>();
			//_container.RegisterType<IMessageSendingService, ConsoleMessageSendingService>(new ContainerControlledLifetimeManager());
			_container.RegisterType<IMessageSendingService, StarConsoleMessageSendingService>();//last wins
			_container.RegisterType<IMessageManager, MessageManager>(new TransientLifetimeManager());
			//_container.RegisterType<IMessageManager, MessageManager>(new ContainerControlledLifetimeManager());
		}

		static void Main(string[] args)
		{
			Bootstrap();
			var messageManager = _container.Resolve<IMessageManager>();
			QueueMessages(messageManager);
			var messageManager2 = _container.Resolve<IMessageManager>();
			QueueMessages(messageManager2);

			if (messageManager == messageManager2)
			{
				Console.WriteLine("Got the same manager twice");
			}
			else
			{
				Console.WriteLine("Got different instances of the manager");
			}
			Console.ReadLine();
		}

		private static void QueueMessages(IMessageManager messageManager)
		{
			var messages = Enumerable.Range(1, 10).Select(i => new Message(string.Format("Hello World! ({0})", i)));
			foreach (var message in messages)
			{
				messageManager.Add(message);
			}
		}
	}
}
