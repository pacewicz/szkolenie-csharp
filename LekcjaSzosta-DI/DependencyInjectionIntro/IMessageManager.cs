using System.Collections.Generic;
using System.Linq;

namespace DependencyInjectionIntro
{
	/// <summary>
	/// Message manager:
	/// -Add (send)
	/// -Save (to a file)
	/// -GetAll (from the underlying collection)
	/// </summary>
	public interface IMessageManager
	{
		void Add(Message message);
		void Save();
		IEnumerable<Message> GetAll();
	}

	public class MessageManager : IMessageManager
	{
		private readonly List<Message> _messages = new List<Message>();

		private readonly IMessageSendingService _sendingService;
		private readonly IMessagePersistenceService _messagePersistenceService;

		public MessageManager(IMessageSendingService sendingService,
			IMessagePersistenceService messagePersistenceService)
		{
			_sendingService = sendingService;
			_messagePersistenceService = messagePersistenceService;
		}

		public void Add(Message message)
		{
			_messages.Add(message);
			_sendingService.Send(message);
		}

		public void Save()
		{
			_messagePersistenceService.SaveAll(_messages);
		}

		public IEnumerable<Message> GetAll()
		{
			return _messages.ToList();
		} 
	}
}