﻿using AutoMapper;
using WeatherStation.App.ViewModel;
using WeatherStation.Core.Entities;

namespace WeatherStation.App
{
	public static class AutoMapperBootstrapper
	{
		public static void InitializeMappings()
		{
			Mapper.Initialize(cfg =>
			{
				cfg.CreateMap<CurrentTemperatureReading, ReadingPO>()
					.ReverseMap();
			});
		}
	}
}
