﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using WeatherStation.App.Services;
using WeatherStation.Core.Services;

namespace WeatherStation.App
{
	class AppBindings: NinjectModule
	{
		public override void Load()
		{
			Bind<IFileAccessService>().To<WindowsFileAccessService>();
		}
	}
}
