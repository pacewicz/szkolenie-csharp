﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Windows.Input;
using AutoMapper;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Ninject.Infrastructure.Language;
using WeatherStation.Core.Entities;
using WeatherStation.Core.Extensions;
using WeatherStation.Core.Services;

namespace WeatherStation.App.ViewModel
{
	public class MainViewModel : ViewModelBase
	{
		private readonly IDataAccessService _dataAccessService;
		private ObservableCollection<ReadingPO> _readings;
		private ICommand _refreshCommand;
		private ICommand _addReadingsCommand;
		private Random _random;
		IDisposable _secondUpdates;
		private IDisposable _addUpdates;
		private ReaderWriterLockSlim _rwLock;

		public MainViewModel(IDataAccessService dataAccessService)
		{
			_dataAccessService = dataAccessService;
			RefreshCommand = new RelayCommand(RefreshAction);
			AddReadingsCommand = new RelayCommand(AddReadingsAction);
			AutoMapperBootstrapper.InitializeMappings();
			_random = new Random();
			_rwLock = new ReaderWriterLockSlim();
		}

		private void AddReadingsAction()
		{
			_addUpdates = Observable.Interval(TimeSpan.FromMilliseconds(1000))
				.Subscribe(l => AddInternal());
			AddInternal();
		}

		private void AddInternal()
		{
			var readings = new List<CurrentTemperatureReading>()
			{
				new CurrentTemperatureReading()
				{
					CityName = "Warszawa",
					Temperature = _random.NextDouble()*30,
					Timestamp = DateTime.Now
				},
				new CurrentTemperatureReading()
				{
					CityName = "Kraków",
					Temperature = _random.NextDouble()*25,
					Timestamp = DateTime.Now
				},
				new CurrentTemperatureReading()
				{
					CityName = "Suwałki",
					Temperature = _random.NextDouble()*-42,
					Timestamp = DateTime.Now
				},
				new CurrentTemperatureReading()
				{
					CityName = "Katowice",
					Temperature = _random.NextDouble()*25,
					Timestamp = DateTime.Now.AddDays(-7)
				},
			};
			_dataAccessService.SaveReadings(readings);
		}

		private void RefreshAction()
		{
			_secondUpdates = Observable.Interval(TimeSpan.FromMilliseconds(100))
				.Subscribe(l => RefreshInternal());
		}

		private void RefreshInternal()
		{
			var rawData = _dataAccessService.GetAllReadings();
			//var presentationObjects = Mapper.Map<List<ReadingPO>>(rawData);
			var presentationObjects = rawData.Map<List<ReadingPO>>();
			var presentationReadyData = presentationObjects.Filter(r => r.Timestamp > DateTime.Now.AddDays(-1));//same
			//var presentationReadyData = MyLinq.Filter(presentationObjects, r => r.Timestamp > DateTime.Now.AddDays(-1));//same
			Readings = new ObservableCollection<ReadingPO>(presentationObjects);
		}

		public ICommand RefreshCommand
		{
			get { return _refreshCommand; }
			set { Set(ref _refreshCommand, value); }
		}

		public ICommand AddReadingsCommand
		{
			get { return _addReadingsCommand; }
			set { Set(ref _addReadingsCommand, value); }
		}

		public ObservableCollection<ReadingPO> Readings
		{
			get { return _readings; }
			set { Set(ref _readings, value); }
		}

	}
}