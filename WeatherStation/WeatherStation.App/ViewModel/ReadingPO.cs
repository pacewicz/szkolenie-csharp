﻿using System;
using GalaSoft.MvvmLight;

namespace WeatherStation.App.ViewModel
{
	public class ReadingPO: ObservableObject
	{
		private string _cityName;
		private double _temperature;
		private DateTime _timestamp;

		public string CityName
		{
			get { return _cityName; }
			set { Set(ref _cityName, value); }
		}

		public double Temperature
		{
			get { return _temperature; }
			set { Set(ref _temperature, value); }
		}

		public DateTime Timestamp
		{
			get { return _timestamp; }
			set { Set(ref _timestamp, value); }
		}

		public override string ToString()
		{
			return $"{CityName} {Temperature}";
		}
	}
}