﻿using System.IO;
using WeatherStation.Core.Services;

namespace WeatherStation.App.Services
{
	public class WindowsFileAccessService: IFileAccessService
	{
		public string GetFile(string name)
		{
			var fileExists = File.Exists(name);
			if (fileExists == false)
			{
				return string.Empty;
			}
			return File.ReadAllText(name);
		}

		public void SaveFile(string name, string data)
		{
			if(File.Exists(name))
			{
				File.Delete(name);
			}
			File.WriteAllText(name, data);
		}
	}
}