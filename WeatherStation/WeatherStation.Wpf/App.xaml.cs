﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using Ninject;
using WeatherStation.Core.Services;

namespace WeatherStation.Wpf
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			//this is not production-ready!
			var kernel = new StandardKernel();
			var thisAssembly = Assembly.GetExecutingAssembly();
			var referencedAssemblies = thisAssembly.GetReferencedAssemblies()
				.Where(a => a.FullName.Contains("WeatherStation"))
				.Select(a => Assembly.Load(a)).ToList();
			referencedAssemblies.Add(thisAssembly);
			kernel.Load(referencedAssemblies);
		}
	}
}
