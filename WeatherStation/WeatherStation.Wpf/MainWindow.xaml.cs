﻿using System.Windows;
using WeatherStation.App.Services;
using WeatherStation.App.ViewModel;
using WeatherStation.Core.Services.Impl;

namespace WeatherStation.Wpf
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			//not production-ready!
			var fileAccess = new WindowsFileAccessService();
			var dataAccess = new DataAccessService(fileAccess);
			DataContext = new MainViewModel(dataAccess);
		}
	}
}
