﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace WeatherStation.Core.Extensions
{
	public static class MyLinq
	{
		public static List<TItem> Filter<TItem>(this IEnumerable<TItem> list, Predicate<TItem> predicate)
		{
			var results = new List<TItem>();
			foreach (var item in list)
			{
				var pass = predicate(item);//same
				//var pass = predicate.Invoke(item);//same

				if (pass)
				{
					results.Add(item);
				}
			}

			return results;
		}

		private static void Test()
		{
			Func<int, bool> x = MyLinq.IsLessThanFive;
			var a = new object();
			Func<int, bool> y =
				(int number) =>
				{
					var z = a;
					return true;
				};

			Predicate<int> il =
				(int number) => number < 5;//same
			Func<int, bool> il2 =
				number => number < 5;//same
			Func<int, bool> il3 =
				(int number) =>
				{
					return number < 5;
				}; //same

		}


		private static bool IsLessThanFive(int number)//same
		{
			return number < 5;
		}
	}
}