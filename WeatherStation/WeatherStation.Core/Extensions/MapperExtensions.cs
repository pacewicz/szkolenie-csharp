﻿using AutoMapper;

namespace WeatherStation.Core.Extensions
{
	public static class MapperExtensions
	{
		public static TDestination Map<TDestination>(this object obj)
		{
			return Mapper.Map<TDestination>(obj);
		} 
	}
}