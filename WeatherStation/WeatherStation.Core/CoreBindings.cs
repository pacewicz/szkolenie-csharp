﻿using Ninject.Modules;
using WeatherStation.Core.Services;
using WeatherStation.Core.Services.Impl;

namespace WeatherStation.Core
{
	public class CoreBindings: NinjectModule
	{
		public override void Load()
		{
			Bind<IDataAccessService>().To<DataAccessService>();
		}
	}
}