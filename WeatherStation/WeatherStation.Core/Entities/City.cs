﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherStation.Core.Entities
{
	public class CurrentTemperatureReading
	{
		public string CityName { get; set; }
		public double Temperature { get; set; }
		public DateTime Timestamp { get; set; }
	}
}
