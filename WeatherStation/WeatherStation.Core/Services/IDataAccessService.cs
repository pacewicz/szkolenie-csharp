﻿using System.Collections.Generic;
using WeatherStation.Core.Entities;

namespace WeatherStation.Core.Services
{
	public interface IDataAccessService
	{
		IEnumerable<CurrentTemperatureReading> GetAllReadings();
		void SaveReadings(IEnumerable<CurrentTemperatureReading> newReadings);
	}
}
