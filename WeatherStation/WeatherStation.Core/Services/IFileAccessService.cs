﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherStation.Core.Services
{
	public interface IFileAccessService
	{
		string GetFile(string name);
		void SaveFile(string name, string data);
	}
}
