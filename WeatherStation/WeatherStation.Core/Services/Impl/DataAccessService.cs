﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;
using WeatherStation.Core.Entities;

namespace WeatherStation.Core.Services.Impl
{
	public class DataAccessService: IDataAccessService
	{
		private readonly IFileAccessService _fileAccessService;
		private const string DbFileName = "readings.json";
		private readonly ReaderWriterLockSlim _rwLock;

		public DataAccessService(IFileAccessService fileAccessService)
		{
			_fileAccessService = fileAccessService;
			_rwLock = new ReaderWriterLockSlim();
		}

		public IEnumerable<CurrentTemperatureReading> GetAllReadings()
		{
			_rwLock.EnterReadLock();
			var fileContents = _fileAccessService.GetFile(DbFileName);
			var readings = JsonConvert.DeserializeObject<List<CurrentTemperatureReading>>(fileContents);
			_rwLock.ExitReadLock();
			if (readings == null)
			{
				return new List<CurrentTemperatureReading>();
			}
			return readings;
		}

		public void SaveReadings(IEnumerable<CurrentTemperatureReading> newReadings)
		{
			var existingReadings = GetAllReadings().ToList();

			var common = existingReadings.Where(e => newReadings.Any(r => r.CityName == e.CityName)).ToList();
			
			foreach (var reading in common)
			{
				existingReadings.Remove(reading);
			}
			existingReadings.AddRange(newReadings);

			var serializedData = JsonConvert.SerializeObject(existingReadings);
			_rwLock.EnterWriteLock();
			_fileAccessService.SaveFile(DbFileName, serializedData);
			_rwLock.ExitWriteLock();
		}
	}
}
