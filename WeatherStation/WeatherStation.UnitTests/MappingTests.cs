﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using NUnit.Framework;
using WeatherStation.App;
using WeatherStation.App.ViewModel;
using WeatherStation.Core.Entities;
using WeatherStation.Core.Extensions;

namespace WeatherStation.UnitTests
{
	[TestFixture]
	public class MappingTests
	{
		[TestFixtureSetUp]
		public void TfSetUp()
		{
			AutoMapperBootstrapper.InitializeMappings();
		}

		[Test]
		public void dto_to_po()
		{
			var dto = new CurrentTemperatureReading()
			{
				CityName = "Olsztyn",
				Temperature = 25,
				Timestamp = DateTime.Now
			};

			var po = Mapper.Map<ReadingPO>(dto);
			AssertPropertiesEqual(dto, po);
		}

		[Test]
		public void po_to_dto()
		{
			var po = new ReadingPO()
			{
				CityName = "Olsztyn",
				Temperature = 25,
				Timestamp = DateTime.Now
			};

			var dto = po.Map<CurrentTemperatureReading>();
			AssertPropertiesEqual(po, dto);
		}

		public void AssertPropertiesEqual<T1, T2>(T1 o1, T2 o2) where T1 : class
			where T2 : class
		{
			var type1 = o1.GetType();
			var props1 = type1.GetProperties();
			var type2 = o2.GetType();
			var props2 = type2.GetProperties();

			Assert.AreEqual(props1.Length, props2.Length);

			foreach (var prop in props1)
			{
				var otherSide = props2.FirstOrDefault(p => p.Name == prop.Name);
				Assert.NotNull(otherSide);
			}

			foreach (var prop in props1)
			{
				var prop2 = props2.First(p => p.Name == prop.Name);
				var v1 = prop.GetValue(o1);
				var v2 = prop2.GetValue(o2);

				Assert.AreEqual(v1, v2);
			}
		}
	}
}
