﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherStation.Core.Entities;
using WeatherStation.Core.Services;
using WeatherStation.Core.Services.Impl;

namespace WeatherStation.UnitTests
{
	[TestFixture]
	public class DataAccessServiceTests
	{
		const string DbFileName = "TestData/TestInput.json";
		[TestFixtureSetUp]
		public void TfSetUp()
		{

		}

		[SetUp]
		public void SetUp()
		{

		}

		[Test]
		public void reads_data()
		{
			
			var file = File.ReadAllText(DbFileName);
			var fileAccessService = NSubstitute.Substitute.For<IFileAccessService>();
			fileAccessService.GetFile(Arg.Any<string>())
				.Returns(file);

			var sut = new DataAccessService(fileAccessService);
			var data = sut.GetAllReadings().ToList();

			Assert.NotNull(data);
			Assert.AreEqual(2, data.Count);
		}

		[Test]
		public void saves_data()
		{
			//Arrange
			var file = File.ReadAllText(DbFileName);
			var fileAccessService = Substitute.For<IFileAccessService>();
			fileAccessService.GetFile(Arg.Any<string>())
				.Returns(file);
			var sut = new DataAccessService(fileAccessService);

			//Act
			sut.SaveReadings(new CurrentTemperatureReading[0]);

			//Assert
			fileAccessService.Received(1).SaveFile(Arg.Any<string>(), Arg.Any<string>());
		}
	}
}
