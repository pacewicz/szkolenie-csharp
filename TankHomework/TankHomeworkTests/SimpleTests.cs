﻿using System;
using System.Linq;
using NUnit.Framework;
using TankHomework;

namespace TankHomeworkTests
{
	[TestFixture]
	public class SimpleTests
	{
		private Mechanic _mechanic;

		[SetUp]
		public void Init()	
		{
			_mechanic = new Mechanic();
		}

		[Test]
		public void cannot_fix_good_tank()
		{
			var tank = new Tank();
			Assert.Throws<Exception>(() => _mechanic.Repair(tank));
		}

		[Test]
		public void hp_cannot_be_negative()
		{
			var tank = new Tank();
			Assert.Throws<Exception>(() => tank.Hit(tank.Health*2));
		}

		[Test]
		public void hp_equals_100_after_repairs()
		{
			var tank = new Tank();
			tank.Hit(96);
			_mechanic.Repair(tank);
			Assert.AreEqual(100, tank.Health);
		}

		[Test]
		public void hit_received_is_fired()
		{
			var tank = new Tank();
			bool eventFired = false;
			int healthAfterHit = 0;
			tank.HitReceived += (sender, currentHealth) => 
			{
				eventFired = true;
				healthAfterHit = currentHealth;
			};
			var initialHealth = tank.Health;
			const int hitVal = 50;
			tank.Hit(hitVal);
			Assert.That(eventFired);
			Assert.AreEqual(initialHealth-hitVal, healthAfterHit);
		}

		[Test]
		public void cannot_fix_tank_with_hp_less_than_1()
		{
			var tank = new Tank();
			tank.Hit(tank.Health);
			Assert.Throws<Exception>(() => _mechanic.Repair(tank));
		}

		[Test]
		public void hit_decreases_hp_properly()
		{
			var tank = new Tank();
			var initialHealth = tank.Health;
			tank.Hit(10);
			Assert.AreEqual(initialHealth-10, tank.Health);
		}

		[Test]
		public void bullet_fired_event_is_fired()
		{
			var tank = new Tank();
			var firedGuid = Guid.Empty;
			tank.BulletFired += (sender, guid) => firedGuid = guid;
			tank.Fire();
			Assert.AreNotEqual(Guid.Empty, firedGuid);
		}

		[Test]
		public void fired_bullet_removed_from_stack()
		{
			var tank = new Tank();
			var firedGuid = Guid.Empty;
			tank.BulletFired += (sender, guid) => firedGuid = guid;
			tank.Fire();
			if(tank.Bullets.GroupBy(b => b.SerialNumber).Any(g => g.Count() > 1))
			{
				Assert.Inconclusive("Duplicate guids!");
				//Let me know if you get this! Pic or didn't happen!
			}
			Assert.AreNotEqual(Guid.Empty, firedGuid);
			Assert.That(tank.Bullets.All(b => b.SerialNumber != firedGuid));
		}

		[Test]
		public void no_unidentified_bullets()
		{
			var tank = new Tank();
			Assert.That(tank.AllBulletsPassTest(b => b.SerialNumber != Guid.Empty));
		}

		[Test]
		public void firing_with_no_bullets_does_not_throw()
		{
			var tank = new Tank();
			var numBullets = tank.Bullets.Count;
			for (int i = 0; i <= numBullets; i++)
			{
				Assert.DoesNotThrow(() => tank.Fire());
			}
			Assert.AreEqual(0, tank.Bullets.Count);
		}
	}
}
