using System;
using TankHomework;

namespace TankHomeworkTests
{
	public static class TankExtensions
	{
		public static bool AllBulletsPassTest(this Tank tank, Predicate<Bullet> p)
		{
			foreach (var bullet in tank.Bullets)
			{
				if (p(bullet) == false)
				{
					return false;
				}
			}
			return true;
		}
	}
}