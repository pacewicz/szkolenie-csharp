﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("TankHomeworkTests")]
namespace TankHomework
{
	public class Tank
	{
		public int Health { get; set; }
		internal readonly Stack<Bullet> Bullets;
		public const int MaxHealth = 100;

		public bool IsBroken
		{
			get { return Health < 5; }
		}

		public event EventHandler<int> HitReceived;
		public event EventHandler<Guid> BulletFired;

		public Tank()
		{
			Health = 100;
			Bullets = new Stack<Bullet>(Enumerable.Range(1, 10).Select(i => new Bullet()));
		}

		public void Fire()
		{
			if (Bullets.Count > 0)
			{
				var b = Bullets.Pop();
				if (BulletFired != null)
					BulletFired(this, b.SerialNumber);
			}
		}

		public void Hit(int val)
		{
			if (val > Health)
			{
				throw new Exception("Unbearable damage.");
			}
			Health -= val;
			if (HitReceived != null)
				HitReceived(this, Health);
		}
	}

	public class Bullet
	{
		public Guid SerialNumber { private set; get; }

		public Bullet()
		{
			SerialNumber = Guid.NewGuid();
		}
	}

	public class Mechanic
	{
		public void Repair(Tank tank)
		{
			if (tank.Health < 1)
			{
				throw new Exception("Cannot repair a destroyed tank.");
			}
			if (tank.Health == Tank.MaxHealth)
			{
				throw new Exception("Cannot repair an undamaged tank.");
			}
			tank.Health += Tank.MaxHealth-tank.Health;
		}
	}

	public class ImportedTank: Tank
	{
		public ImportedTank()
		{
			Health = 4;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			var tank = new ImportedTank();
			var mechanic = new Mechanic();
			Console.WriteLine("Broken: {0}", tank.IsBroken);
			mechanic.Repair(tank);
			Console.WriteLine("After repairs: {0}", tank.IsBroken);
		}
	}
}
