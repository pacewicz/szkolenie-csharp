﻿using System;

namespace EventsTesting
{
	public class Tank
	{
		private readonly Cannon _cannon;
		private int _number;
		public Cannon Cannon { get { return _cannon; } }
		public int Number { get { return _number; } }
		public event EventHandler<string> CannonFired;


		public Tank(Cannon cannon, int number)
		{
			_cannon = cannon;
			_number = number;
		}

		public void FireCannon()
		{
			Console.WriteLine("Ready? Set? Fire!");
			_cannon.Fire();
			if (CannonFired != null)
			{
				CannonFired(this, "Yes!");
			}
		}

		public void FireCannon(string additionalMessage)
		{
			Console.WriteLine(additionalMessage);
			FireCannon();
		}
	}

	public abstract class Cannon
	{
		public abstract int Diameter { get; }

		public void Fire()
		{
			Console.WriteLine("{0} boom!", Diameter);
		}
	}

	public class SmallCannon: Cannon
	{
		public override int Diameter
		{
			get { return 10; }
		}
	}

	public class BigCannon : Cannon
	{
		public override int Diameter
		{
			get { return 200; }
		}
	}

	class Program
	{
		public static EventHandler<string> MessageReceivedEventHandler;
		//EventHandler<T> is a convenience type available since .NET 4

		static void Main(string[] args)
		{
			MessageReceivedEventHandler += (sender, s) => Console.WriteLine("Tank {0} has fired!", ((Tank)sender).Number);
			MessageReceivedEventHandler += (sender, s) => Console.WriteLine("-----------------");


			var tank = new Tank(new BigCannon(), 102);//Rudy

			tank.CannonFired += MessageReceivedEventHandler;
			tank.CannonFired += (sender, s) => Console.WriteLine("Another dummy handler!");

			tank.FireCannon();
		}
	}
}
