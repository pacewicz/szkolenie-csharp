﻿using System;

namespace DelegatesTesting
{
	class Program
	{
		public delegate void MessageBroadcaster(string message);//type definition!

		public class SimilarToThis
		{
		}

		static void Main(string[] args)
		{
			#region add and subtract
			MessageBroadcaster broadcaster = s => Console.WriteLine("1 {0}", s);
			broadcaster += s => Console.WriteLine("2 {0}", s);
			broadcaster += CustomBroadcaster;

			broadcaster("Hello, World!");

			broadcaster -= CustomBroadcaster;

			broadcaster("Foo Bar");
			#endregion

			#region evil case
			broadcaster = s => Console.WriteLine("Ha! Evil!");
			Console.WriteLine("-------------");
			broadcaster("Hello?");
			#endregion

		}

		private static void CustomBroadcaster(string message)
		{
			Console.WriteLine("custom0 {0}", message);
		}
	}
}
