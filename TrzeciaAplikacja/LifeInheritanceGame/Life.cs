﻿using System;

namespace LifeInheritanceGame
{
	#region Once upon a time, there was the concept of life...
	public abstract class Life
	{
		
	}

	#region ...And the first living creature came to life...
	public class FirstSwimmingLife : Life
	{
		public virtual void IntroduceYourself()
		{
			Console.WriteLine("I can swim!");
		}
	}

	#region ...Then, it evolved...
	public class SecondSwimmingLife: FirstSwimmingLife
	{
		public override void IntroduceYourself()
		{
			Console.WriteLine("I can swim fast!");
		}
	}

	#region ...and became even more perfect...
	public class IdealSwimmingLife : SecondSwimmingLife
	{
		
	}

	#region ...Then, it grew legs...
	public class FirstSwimmingLifeWithLegs : IdealSwimmingLife
	{
		public new virtual void IntroduceYourself()
		{
			Console.WriteLine("I have legs!");
		}
	}

	#region ...And began to walk...
	public class WalkingLife : FirstSwimmingLifeWithLegs
	{
		public override void IntroduceYourself()
		{
			base.IntroduceYourself();
			Console.WriteLine("I can walk!");
		}
	}

	#region ...Fast forward...
	public class Human: WalkingLife
	{
		private int _happiness;
		private string _name;
		private string _surname;

		public event EventHandler<int> HappinessChanged;

		public int Happiness
		{
			get { return _happiness; }
		}

		public Human(string name, string surname)
		{
			_name = name;
			_surname = surname;
		}

		public override void IntroduceYourself()
		{
			Console.WriteLine("I'm a hooman!");
		}

		public string Name { get { return _name; } }
		public string Surname { get { return _surname; } }

		public void IncHappinness()
		{
			++_happiness;
			if (HappinessChanged != null)
			{
				HappinessChanged(this, _happiness);
			}
		}
	}
	#endregion
	#endregion
	#endregion
	#endregion
	#endregion
	#endregion
	#endregion
}