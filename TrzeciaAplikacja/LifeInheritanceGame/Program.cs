﻿using System;

namespace LifeInheritanceGame
{
	public static class HumanExtensions
	{
		//FirstSwimmingLifeWithLegs
		public static void ExtendedIntroduction(this FirstSwimmingLifeWithLegs h)
		{
			if(h is Human)
				Console.WriteLine(((Human) h).Name+":");
			h.IntroduceYourself();
			Console.WriteLine("------------");
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			#region finished stuff
			//var second = new SecondSwimmingLife();
			//second.IntroduceYourself();
			//var withLegs = new FirstSwimmingLifeWithLegs();
			//withLegs.IntroduceYourself();

			//var walking = new WalkingLife();
			//walking.IntroduceYourself();

			//var ideal = new IdealSwimmingLife();
			//ideal.IntroduceYourself();

			var human = new Human("John", "Kowalski");
			human.ExtendedIntroduction();
			//FirstSwimmingLifeWithLegs human2 = new Human("Keith", "David");
			//human2.ExtendedIntroduction();
			//var human3 = new Human("Kinzie", "Kensington");
			//human3.ExtendedIntroduction();

			//human3.HappinessChanged += (h, v) => Console.WriteLine(((Human)h).Name+"'s happiness increased to "+v);
			//human3.IncHappinness();
			//human3.IncHappinness();
			#endregion

		}
	}
}
