﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionsDemo
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Enter an integer:");
			//int integer = 0;
			//do
			//{
			//	try
			//	{
			//		var str = Console.ReadLine();
			//		integer = int.Parse(str);
			//		Console.WriteLine("integer: {0}", integer);
			//	}
			//	catch (System.Exception)
			//	{
			//		Console.WriteLine("I said an integer. Try again:");
			//	}
			//} while (integer == 0);

			var str = Console.ReadLine();
			int integer = 0;
			if(int.TryParse(str, out integer))
			{
				Console.WriteLine("integer: {0}", integer);
			}
		}
	}
}
