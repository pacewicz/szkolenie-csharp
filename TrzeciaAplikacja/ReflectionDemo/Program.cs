﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionDemo
{
	class Program
	{
		static void Main(string[] args)
		{
			var human = new Human("Matt", "Miller");
			var humanPropertiesInfo = human.GetType().GetProperties();
			foreach (var propertyInfo in humanPropertiesInfo)
			{
				Console.WriteLine("{0} of type {1} named {2}", propertyInfo.MemberType,
					propertyInfo.PropertyType, propertyInfo.Name);
			}
		}
	}
}
