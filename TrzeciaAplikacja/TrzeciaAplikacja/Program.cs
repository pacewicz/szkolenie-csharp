﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrzeciaAplikacja
{
	class Program
	{
		interface IComputationService
		{
			void DoWork();
		}

		interface IDataReaderService
		{
			void DoWork();
		}

		class VersatileService: IComputationService, IDataReaderService
		{
			void IComputationService.DoWork()
			{
				
			}

			void IDataReaderService.DoWork()
			{
				
			}
		}

		static void Main(string[] args)
		{
			var service = new VersatileService();

			((IComputationService)service).DoWork();
			((IDataReaderService)service).DoWork();

			IComputationService explicitTypeSpecService = new VersatileService();
			explicitTypeSpecService.DoWork();


			var array = new[] {1, 2, 3, 4};
			//array.Length
			Console.WriteLine(((ICollection<int>)array).Count);
			array.Where(i => i > 2);
		}
	}
}
