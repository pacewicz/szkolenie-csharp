﻿namespace DrugaAplikacja
{
	public class Human
	{
		private int _happiness;
		private string _name;
		private string _surname;

		public int Happiness
		{
			get { return _happiness; }
		}

		public Human(string name, string surname)
		{
			_name = name;
			_surname = surname;
		}

		public string Name { get { return _name; } }
		public string Surname { get { return _surname; } }

		public void IncHappinness()
		{
			++_happiness;
		}
	}

	public enum SoldierRank
	{
		Private = 0,
		Specialist = 1,
		Corporal = 2,
		Sergeant,//and so on, if you wish
		Captain,
		Major,
		Colonel,
		General,
	}

	public class Soldier : Human
	{
		public SoldierRank Rank { get; set; }
		private int weight;

		public Soldier(string name, string surname) : base(name, surname)
		{
		}

		public Soldier(string name, string surname, int weight): this(name, surname)
		{
			this.weight = weight;
		}
	}
}