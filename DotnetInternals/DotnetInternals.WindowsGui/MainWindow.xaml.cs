﻿using DotnetInternals.App.Helpers;
using DotnetInternals.App.Message;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;

namespace DotnetInternals.WindowsGui
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			UpdateDataContext();
			Application.Current.DispatcherUnhandledException += CurrentDispatcherUnhandledException;
			AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;
			this.Dispatcher.UnhandledException += DispatcherUnhandledException;
			Messenger.Default.Register(this, (UpdateViewModelRequest _) => UpdateDataContext());
		}

		private void DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs args)
		{
			args.Handled = true;
			ShowMessage(args.Exception);
		}

		private void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs args)
		{
			ShowMessage((Exception)args.ExceptionObject);
		}

		private void CurrentDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs args)
		{
			args.Handled = true;
			ShowMessage(args.Exception);
		}

		private void ShowMessage(Exception e, [CallerMemberName]string location = null)
		{
			MessageBox.Show($"{e.Message}\n------------{e.StackTrace}", location);
		}

		private void UpdateDataContext()
		{
			DataContext = MainViewModelSwitcher.GetNextViewModelInstance();
		}
	}
}
