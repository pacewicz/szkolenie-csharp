﻿using DotnetInternals.Core;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace DotnetInternals.Windows
{
	class Program
	{
		static void Main(string[] args)
		{
			AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) => Debugger.Break();
			TaskScheduler.UnobservedTaskException += (sender, eventArgs) => Debugger.Break();

			Throw1();

			Console.ReadKey();
		}

		private static void Throw1()
		{
			var asyncIntro = new AsyncIntro();
			asyncIntro.ArraysThreadSafety();
		}
	}
}
