﻿using NUnit.Framework;
using DotnetInternals.Core;

namespace DotnetInternals.UnitTests
{
	[TestFixture]
    public class AsyncTests
    {
		[Test]
		public async void list_thread_safety()
		//public async void ListThreadSafety()
		{
			var sut = new AsyncIntro();
			Assert.DoesNotThrow(() => sut.ListThreadSafety());
		}
    }
}
