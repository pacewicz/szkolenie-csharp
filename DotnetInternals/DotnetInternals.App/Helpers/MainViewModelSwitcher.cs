﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DotnetInternals.App.Message;
using DotnetInternals.App.ViewModel;

namespace DotnetInternals.App.Helpers
{
	public static class MainViewModelSwitcher
	{
		private static bool _isInitialized;
		private static List<Type> _viewModelTypes;
		private static int _currentIdx;
		public static MainViewModelBase CurrentInstance { get; private set; }

		public static void Init()
		{
			_isInitialized = true;
			var appAssembly = Assembly.GetAssembly(typeof(MainViewModelSwitcher));
			_viewModelTypes = appAssembly.GetExportedTypes()
				.Where(t => t.IsSubclassOf(typeof(MainViewModelBase)) && t.IsAbstract == false)
				.OrderBy(t => t.GetCustomAttribute<ViewModelOrderAttribute>()?.Order)
				.ThenBy(t => t.FullName)
				.ToList();
			_currentIdx = 0;
		}

		public static MainViewModelBase GetNextViewModelInstance()
		{
			if (_isInitialized == false)
			{
				Init();
			}
			if (_currentIdx >= _viewModelTypes.Count)
			{
				return CurrentInstance;
			}
			CurrentInstance = (MainViewModelBase)Activator.CreateInstance(_viewModelTypes[_currentIdx]);
			CurrentInstance.Init();
			_currentIdx++;
			return CurrentInstance;
		}
	}
}