﻿using System;

namespace DotnetInternals.App.Helpers
{
	[AttributeUsage(AttributeTargets.Class)]
	public class ViewModelOrderAttribute : Attribute
	{
		public int Order { get; }

		public ViewModelOrderAttribute(int order)
		{
			Order = order;
		}
	}
}