using DotnetInternals.App.Helpers;
using DotnetInternals.App.Message;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Threading;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace DotnetInternals.App.ViewModel
{
	public abstract class MainViewModelBase : ViewModelBase
	{
		protected MainViewModelBase()
		{
			_switchToNextViewModelCommand = new RelayCommand(SwitchToNextViewModelAction);
			ListItems = new ObservableCollection<string>();
			_addItemsCommand = new RelayCommand(AddItemsAction);
			DispatcherHelper.Initialize();
		}

		protected abstract void AddItemsAction();

		private ObservableCollection<string> _listItems;
		public ObservableCollection<string> ListItems
		{
			get { return _listItems; }
			set { Set(ref _listItems, value); }
		}

		#region Less important details

		private void SwitchToNextViewModelAction()
		{
			MessengerInstance.Send(new UpdateViewModelRequest());
		}

		private ICommand _switchToNextViewModelCommand;
		public ICommand SwitchToNextViewModelCommand
		{
			get { return _switchToNextViewModelCommand; }
			set { Set(ref _switchToNextViewModelCommand, value); }
		}
		protected void AddItemToList(string content)
		{
			DispatcherHelper.CheckBeginInvokeOnUI(() => { _listItems.Add(content); });
		}

		private ICommand _addItemsCommand;
		public ICommand AddItemsCommand
		{
			get { return _addItemsCommand; }
			set { Set(ref _addItemsCommand, value); }
		}

		private string _statusText;
		public string StatusText
		{
			get { return _statusText; }
			set { Set(ref _statusText, value); }
		}
		public virtual void Init()
		{
			var type = GetType();
			var orderAttribute = type.CustomAttributes.First(a => a.AttributeType == typeof(ViewModelOrderAttribute));
			var vmOrder = (int)orderAttribute.ConstructorArguments.First().Value;
			StatusText = $"ViewModel #{vmOrder} : {type.Name}";
		}
		#endregion
	}
}