using DotnetInternals.App.Helpers;
using DotnetInternals.Core;
using GalaSoft.MvvmLight.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DotnetInternals.App.ViewModel
{
	[ViewModelOrder(1)]
	public class SimpleAdd : MainViewModelBase
	{
		protected override void AddItemsAction()
		{
			Parallel.ForEach(Enumerable.Range(0, 100), i => ListItems.Add($"Item {i}"));
		}
	}

	[ViewModelOrder(2)]
	public class SafeAdd : MainViewModelBase
	{
		protected override void AddItemsAction()
		{
			Parallel.ForEach(Enumerable.Range(0, 100), i => AddItemToList($"Item {i}"));
		}
	}

	[ViewModelOrder(3)]
	public class AdvancedAdd : MainViewModelBase
	{
		protected override void AddItemsAction()
		{
			Parallel.ForEach(Enumerable.Range(0, 100),
				async i =>
				{
					var threadId = Thread.CurrentThread.ManagedThreadId;
					var value = await DummyTask.ExecuteRandomWithReturnValue();
					var content = $"Item {i} : {value} from thread {threadId}";
					AddItemToList(content);
				});
		}
	}

	[ViewModelOrder(4)]
	public class UnhandledException : MainViewModelBase
	{
		protected override void AddItemsAction()
		{
			Parallel.ForEach(Enumerable.Range(0, 100),
				async i =>
				{
					var threadId = Thread.CurrentThread.ManagedThreadId;
					var value = await DummyTask.ThrowWithProbabilityAndReturn(0.1);
					var content = $"Item {value} from thread {threadId}";
					AddItemToList(content);
				});
		}
	}

	[ViewModelOrder(5)]
	public class UnobservedException : MainViewModelBase
	{
		protected override void AddItemsAction()
		{
			Task.Run(() => Throw());
		}

		private async void Throw()
		{
			await Task.Delay(1);
			throw new Exception();
		}
	}

	[ViewModelOrder(6)]
	public class CaughtException : MainViewModelBase
	{
		protected override void AddItemsAction()
		{
			Task.Run(async () =>
			{
				try
				{
					await ThrowAsync();
				}
				catch (Exception e)
				{
					DispatcherHelper.CheckBeginInvokeOnUI(() => StatusText = e.Message);
				}
			});
		}

		private async Task ThrowAsync()
		{
			await Task.Delay(1);
			throw new Exception();
		}
	}

	[ViewModelOrder(7)]
	public class ListThreadSafety : MainViewModelBase
	{
		protected override void AddItemsAction()
		{
			Task.Run(async () =>
			{
				try
				{
					await ThrowAsync();
				}
				catch (Exception e)
				{
					DispatcherHelper.CheckBeginInvokeOnUI(() => StatusText = e.Message);
				}
			});
		}

		private async Task ThrowAsync()
		{
			await Task.Delay(1);
			throw new Exception();
		}
	}
}
