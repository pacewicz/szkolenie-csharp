﻿using System;
using System.Threading.Tasks;

namespace DotnetInternals.Core
{
	public static class DummyTask
	{
		private static readonly Random Random;
		const int DefaultMaxSeconds = 6;

		static DummyTask()
		{
			Random = new Random();
		}

		public static async Task GetRandom(int maxSecondsToCompletion = DefaultMaxSeconds)
		{
			var seconds = Random.NextDouble() * maxSecondsToCompletion;
			await Task.Delay(TimeSpan.FromSeconds(seconds));
		}

		public static async Task<double> ExecuteRandomWithReturnValue(int maxSecondsToCompletion = DefaultMaxSeconds)
		{
			var seconds = Random.NextDouble() * maxSecondsToCompletion;
			await Task.Delay(TimeSpan.FromSeconds(seconds));
			return seconds;
		}

		public static async Task ThrowWithProbability(double probability = 0.5, int maxSecondsToCompletion = DefaultMaxSeconds)
		{
			var shouldThrow = Random.NextDouble() <= probability;
			var result = await ExecuteRandomWithReturnValue(maxSecondsToCompletion);
			if (shouldThrow)
			{
				throw new Exception();
			}
		}

		public static async Task<double> ThrowWithProbabilityAndReturn(double probability = 0.5, int maxSecondsToCompletion = DefaultMaxSeconds)
		{
			var shouldThrow = Random.NextDouble() <= probability;
			var result = await ExecuteRandomWithReturnValue(maxSecondsToCompletion);
			if (shouldThrow)
			{
				throw new Exception();
			}
			return result;
		}
	}
}