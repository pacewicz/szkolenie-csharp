﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DotnetInternals.Core
{
	public class AsyncIntro
	{
		public void ListThreadSafety()
		{
			var list = new List<int>();
			var locker = new object();

			Parallel.ForEach(Enumerable.Range(0, 2000), i =>
			{
				lock(locker)
				{
					list.Add(i);
				}

			});
			list.Add(list.Count);
		}

		public void ArraysThreadSafety()
		{
			var array = new int[8][];
			var lockers = new object[8];
			for (int i = 0; i < 8; i++)
			{
				lockers[i] = new object();
			}
			Parallel.ForEach(Enumerable.Range(0, 32), i =>
			{
				lock(lockers[i%8])
				{
					array[i % 8] = new int[10];
				}
			});
		}
	}
}
